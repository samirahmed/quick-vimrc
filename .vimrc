
""""""""""""""""""""""""""""""""""""""""""""""
" Environment + Visual Settings
"
"""""""""""""""""""""""""""""""""""""""""""""
syntax enable
set ruler
set number

" Set the highlighting on the Pmenu
highlight PMenu 		term=reverse cterm=bold ctermfg=white ctermbg=black
highlight PMenuSel 		term=reverse cterm=bold ctermfg=red ctermbg=black
"""""""""""""""""""""""""""""""""""""""""
" Basic Navigation and Text Manipulation
"
"""""""""""""""""""""""""""""""""""""""""

" Set the scroll offset to be 10 lines
set scrolloff=10

" Map CTRL-L to go to the last line on the screen
map! <C-L> <ESC>Li

" Map CTRL-E to go to end of line
map! <C-e> <ESC>$a

" Map CTRL-A to go the start of line
map! <C-a> <ESC>0i

" Map CTRL-S to Write command
map! <C-s> <ESC>:wq

" Map CTRL-K to delete after the cursor
map! <C-k> <ESC>lDi

" Map the key bindings k then j to be equal to escape i.e goto normal mode  
"ino kj <esc>l


""""""""""""""""""""""""
" Global Indentation Rules 
"
""""""""""""""""""""""""
set cindent
set smartindent
set autoindent
set tabstop=4
set shiftwidth=4
set cinkeys=0{,0},:,0#,!^F



""""""""""""""""
" Quick Compile
"
""""""""""""""""

" javac
map <LocalLeader>j	<ESC>:!javac %
" gcc
map <Localleader>gcc <ESC>:!gcc %
" run the a.out
map <Localleader>a.out <ESC>:!./a.out


""""""""""""""""""""""""
" FileType Management
""""""""""""""""""""""""
:filetype on
autocmd BufNewFile,BufRead  *.md   source ~/.vim/markdown.vim
autocmd BufNewFile,BufRead  *.md,*.html,*.xml  source ~/.vim/closetag.vim
autocmd BufNewFile,BufRead  *.py  source ~/.vim/python.vim
source ~/.vim/supertab.vim



